#!/bin/bash
function nfgrep() {
#use random strings to prevent overlapping when doing multiple jobs
random=$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w 10 | head -n 1)
grep $@ > /tmp/grep.$random
if [[ "$(cat /tmp/grep.$random)" != "" ]]; then
    cat /tmp/grep.$random
else
    echo
fi
rm -f /tmp/grep.$random
}
awk '/^C:/{printf NR" "}/^$/{print NR}' APKINDEX | while read start end;do head -$end APKINDEX | tail -$(($end - $start)) | nfgrep "^$1:" | sed "s/$1://g" ;done  > $1 
