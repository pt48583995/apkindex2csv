#!/bin/bash
currdt=$(TZ=UTC date +%Y%m%d_%H%M%S) 
for arch in aarch64 armhf armv7 ppc64le riscv64 s390x x86 x86_64 
do
	mkdir $arch
	pushd $arch
	echo -e "Name\tVersion\tDescription\tSize\tInstalled size\tProject URL\tLicense\tOrigin\tBuild time (UTC)\tCommit" > packages_dump_tab_separated_all_$arch_$currdt.csv
	echo -e "Name,Version,Description,Size,Installed size,Project URL,License,Origin,Build time (UTC),Commit" > packages_dump_comma_separated_all_$arch_$currdt.csv
	for repo in main community testing 
	do
		mkdir $repo	
		pushd $repo
		wget -O - https://mirrors.xtom.com.hk/alpine/edge/$repo/$arch/APKINDEX.tar.gz | tar -zx
		echo -e "Name\tVersion\tDescription\tSize\tInstalled size\tProject URL\tLicense\tOrigin\tBuild time (UTC)\tCommit" > packages_dump_tab_separated_$repo_$arch_$currdt.csv
		echo -e "Name,Version,Description,Size,Installed size,Project URL,License,Origin,Build time (UTC),Commit" > packages_dump_comma_separated_$repo_$arch_$currdt.csv
		for part in P V T S I U L o c 
		do
			for i in 1;do grep "^$part\:" APKINDEX | sed "s/$part://g" | while read p;do echo -n "\"" && echo -n "$p" && echo "\"";done;done > $part
		done
		grep "^t\:" APKINDEX | sed "s/t://g" > t
		cat t | while read ut
		do
			TZ=UTC date -d @$ut +%F\ %T >> t_converted
		done
		paste P V T S I U L o t_converted c >> packages_dump_tab_separated_$repo_$arch_$currdt.csv
		paste P V T S I U L o t_converted c >> ../packages_dump_tab_separated_all_$arch_$currdt.csv
		paste -d, P V T S I U L o t_converted c >> packages_dump_comma_separated_$repo_$arch_$currdt.csv
		paste -d, P V T S I U L o t_converted c >> ../packages_dump_comma_separated_all_$arch_$currdt.csv
		rm -f P V T S I U L o t c t_converted APKINDEX DESCRIPTION .SIGN.RSA*
		popd
		done
	popd
done
